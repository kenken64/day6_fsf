/**
 * Created by phangty on 4/7/16.
 */
var ValidationApp = angular.module("ValidationApp", []);

(function(){
    var ValidationCtrl;

    ValidationCtrl = function($http){
        var ctrl = this;

        // ng model for first name
        ctrl.firstName = "";
        // ng model for last Name
        ctrl.lastName = "";
        // ng model for postal code
        ctrl.postalCode = "";
        // ng model for email
        ctrl.email = "";

        // response message
        ctrl.status = {
            message: "",
            code: 0
        };

        ctrl.register = function() {
            $http.post("/register",{
                params : {
                    firstName : ctrl.firstName,
                    lastName  : ctrl.lastName,
                    postalCode : ctrl.postalCode,
                    email : ctrl.email
                }
                }).then(function(){
                    console.info(" Success");
                    ctrl.status.message ="Your registration is complete.";
                    ctrl.status.code = 202;
                }).catch(function(){
                    console.info(" Error");
                    ctrl.status.message = "Your registration failed.";
                    ctrl.status.code = 400;
                });
        };
    };

    ValidationApp.controller("ValidationCtrl", ['$http', ValidationCtrl]);
 })();
